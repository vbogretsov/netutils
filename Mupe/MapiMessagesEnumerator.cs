﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TVS.Mupe;

namespace ExchangeIterator
{
    public class MapiMessagesEnumerator : IEnumerator<MapiEntryId>
    {
        private readonly IEnumerator<MapiEntryId> rows;

        public MapiMessagesEnumerator(
            MapiMsgStore store,
            MapiEntryId folderEntryId)
        {
            rows = GetMessageIds(store, folderEntryId).GetEnumerator();
        }

        public void Dispose()
        {
            rows.Dispose();
        }

        public bool MoveNext()
        {
            bool moveNext = rows.MoveNext();
            Current = rows.Current;
            return moveNext;
        }

        public void Reset()
        {
            throw new NotSupportedException();
        }

        private static IEnumerable<MapiEntryId> GetMessageIds(
            MapiMsgStore store,
            MapiEntryId folderEntryId)
        {
            using (MapiFolder folder = store.OpenFolder(folderEntryId))
            {
                using (MapiTable table = folder.GetContentsTable())
                {
                    table.SetColumns(MapiTag.EntryId);
                    return table.QueryRows().Select(
                        r => r.GetEntryId(MapiTag.EntryId));
                }
            }
        }

        public MapiEntryId Current
        {
            get;
            private set;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }
    }
}
