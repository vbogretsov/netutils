﻿using System;
using System.Collections;
using System.Collections.Generic;
using TVS.Mupe;

namespace ExchangeIterator
{
    public class MapiFoldersEnumerator : IEnumerator<MapiEntryId>
    {
        private readonly MapiMsgStore store;
        private readonly Stack<MapiEntryId> folders;

        public MapiFoldersEnumerator(MapiMsgStore store)
        {
            this.store = store;
            folders = new Stack<MapiEntryId>();
            folders.Push(GetRootFolerEntryId(store));
        }

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            bool moveNext = false;
            if (folders.Count > 0)
            {
                Current = folders.Pop();
                using (MapiFolder folder = store.OpenFolder(Current))
                {
                    foreach (MapiEntryId child in folder.GetFolders())
                    {
                        folders.Push(child);
                    }
                }
                moveNext = true;
            }
            return moveNext;
        }

        public void Reset()
        {
            throw new NotSupportedException();
        }

        private static MapiEntryId GetRootFolerEntryId(MapiMsgStore store)
        {
            using (MapiFolder rootFolder = store.OpenRootFolder(false))
            {
                return rootFolder.GetEntryId(MapiTag.EntryId);
            }
        }

        public MapiEntryId Current
        {
            get;
            private set;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }
    }
}
