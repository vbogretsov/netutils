﻿using System;
using System.Collections;
using System.Collections.Generic;
using TVS.Mupe;

namespace ExchangeIterator
{
    public class ExchangeMailboxEnumerator : IEnumerator<MapiEntryId>
    {
        private readonly MapiMsgStore store;
        private readonly IEnumerator<MapiEntryId> foldersEnumerator;

        private IEnumerator<MapiEntryId> messagesEnumerator;

        public ExchangeMailboxEnumerator(MapiMsgStore store)
        {
            this.store = store;
            foldersEnumerator = new MapiFoldersEnumerator(store);
        }

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            bool moveNext = false;
            if (messagesEnumerator != null && messagesEnumerator.MoveNext())
            {
                moveNext = true;
                Current = messagesEnumerator.Current;
            }
            else
            {
                while (foldersEnumerator.MoveNext())
                {
                    messagesEnumerator = new MapiMessagesEnumerator(
                        store,
                        foldersEnumerator.Current);
                    if (messagesEnumerator.MoveNext())
                    {
                        moveNext = true;
                        Current = messagesEnumerator.Current;
                        break;
                    }
                }
            }
            return moveNext;
        }

        public void Reset()
        {
            throw new NotSupportedException();
        }

        public MapiEntryId Current
        {
            get;
            private set;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }
    }
}
