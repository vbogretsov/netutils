﻿using System;
using System.Threading;

namespace SemaphoreTest
{
    public class ScalableSemaphore
    {
        private readonly object syncRoot;

        private int activeCount;

        public ScalableSemaphore(int bound)
        {
            Bound = bound;
            syncRoot = new object();
        }

        public void Increase()
        {
            lock (syncRoot)
            {
                Bound++;
            }
        }

        public void Decrease()
        {
            lock (syncRoot)
            {
                Bound--;
            }
        }

        public bool TryEnter()
        {
            bool entered = false;
            lock (syncRoot)
            {
                if (activeCount < Bound)
                {
                    activeCount++;
                    entered = true;
                }
            }
            return entered;
        }

        public void Enter()
        {
            bool entered = false;
            while (!entered)
            {
                lock (syncRoot)
                {
                    if (activeCount < Bound)
                    {
                        activeCount++;
                        entered = true;
                    }
                    else
                    {
                        Monitor.Wait(syncRoot);
                    }
                }
            }
        }

        public void Exit()
        {
            lock (syncRoot)
            {
                Monitor.Pulse(syncRoot);
                activeCount--;
            }
        }

        public int Bound
        {
            get;
            private set;
        }
    }
}
