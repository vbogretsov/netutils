﻿using System;
using System.Text;
using TVS.TransVault.Connector.Common.Ev;
using TVS.TransVault.Connector.Common.Ev.Database;
using TVS.TransVault.Shared.BusinessObjects;
using TVS.TransVault.Shared.BusinessObjects.Enums;

namespace EvMessageGathering
{
    public class EvMessageGatherer
    {
        private readonly DatabaseConnection connection;
        private readonly IEvDbConnector dbConnector;

        public EvMessageGatherer(DatabaseConnection connection)
        {
            this.connection = connection;
            dbConnector = EvDbConnector.GetInstance(
                connection,
                MailSystemType.Exchange);
        }

        public EvMessageData GahterMessage(string archiveId, string savesetId)
        {
            EvArchive archive = dbConnector.GetArchive(archiveId); // query 1
            EvVaultStore store = dbConnector.GetVaultStore(archive.VaultStoreId); // query 2
            EvStorageDbConnector storageDbConnector = new EvStorageDbConnector(
                GetStorageConnection(store),
                null);
            EvMessageData message = storageDbConnector.GetMessage(
                archiveId,
                ToGuid(savesetId));
            if (message == null)
            {
                throw new ArgumentException("Message not found");
            }
            return message;
        }

        private DatabaseConnection GetStorageConnection(EvVaultStore store)
        {
            var database = new DatabaseConnection();
            database.Catalog = store.DatabaseName;
            database.Server = store.DatabaseServer;
            database.DatabaseType = DatabaseType.MsSql;
            database.Username = connection.Username;
            database.Schema = connection.Schema;
            database.Password = connection.Password;
            database.NtAuthentication = connection.NtAuthentication;
            return database;
        }

        private static string GetTransactionId(string savesetId)
        {
            int startIndex = savesetId.LastIndexOf('~') + 1;
            return savesetId.Substring(startIndex);
        }

        private static Guid ToGuid(string savesetId)
        {
            string transactionId = GetTransactionId(savesetId);
            StringBuilder guid = new StringBuilder();
            guid.Append(transactionId.Substring(0, 8));
            guid.Append('-');
            guid.Append(transactionId.Substring(8, 4));
            guid.Append('-');
            guid.Append(transactionId.Substring(12, 4));
            guid.Append('-');
            guid.Append(transactionId.Substring(16, 4));
            guid.Append('-');
            guid.Append(transactionId.Substring(20, 12));
            return new Guid(guid.ToString());
        }
    }
}
